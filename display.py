import time
import threading
from fetch import GameFetch
from rgbmatrix import Adafruit_RGBmatrix
import atexit
from tile import NoGameTile
from tile import GameOverviewTile
import Image
import ImageDraw


class SystemOutput:
    gamesJson = None

    def set_games(self, g):
        self.gamesJson = g

    def __init__(self):
        t = threading.Thread(target=self.thread)
        t.daemon = True
        t.start()

    def thread(self):
        while True:
            if self.gamesJson is not None:
                games = self.gamesJson.items()
                for game in games:
                    print('Game: ' + game[0] + ' -> ' + str(game[1]))
            time.sleep(2)
    # The idea will be to check gamesJson once we have hit the end of the games list when displaying it on the board.


class LEDOutput:
    # Configurable stuff ---------------------------------------------------------
    displayWidth = 128  # Matrix size (pixels) -- change for different matrix
    height = 32  # types (incl. tiling).  Other code may need tweaks.
    matrix = Adafruit_RGBmatrix(32, 4)  # rows, chain length
    fps = 20  # Scrolling speed (ish)
    gameFetch = None
    numTiles = displayWidth/32  # assumption that each tile should be a minimum of 32px

    image = Image.new('RGB', (displayWidth, height))
    draw = ImageDraw.Draw(image)

    # Main variables
    gamesJson = None
    nextGameIndex = 0
    tilesAcross = 0
    tileList = []

    def clear_on_exit(self):
        self.matrix.Clear()

    def set_games(self, g):
        self.gamesJson = g

    def __init__(self):
        self.gameFetch = GameFetch()
        atexit.register(self.clear_on_exit)

        self.init_led()

        t = threading.Thread(target=self.led_thread)
        t.daemon = True
        t.start()

    def init_led(self):
        sum_tile_width = 0

        while not self.gameFetch.hasLoaded:
            # this should be done better by having the fetch call this method which will start the led_thread
            time.sleep(1)

        if len(self.gameFetch.getGameInformations()) > 0:
            # make numTiles tiles since we that should be the most that can be displayed at a single time as we cannot
            # guarantee the width of tiles.
            i = 0
            j = i
            game_informations = self.gameFetch.getGameInformations()
            while i < self.numTiles:
                if j >= len(game_informations):
                    j = 0

                game = game_informations[j]
                tile = GameOverviewTile(sum_tile_width, game, self.nextGameIndex, self.draw, self.height)
                tile.draw()
                self.nextGameIndex += 1
                if self.nextGameIndex >= len(game_informations):
                    self.nextGameIndex = 0
                self.tilesAcross += 1
                self.tileList.append(tile)
                sum_tile_width += tile.totalWidth
                j += 1
                i += 1
        else:
            tile = NoGameTile()
            tile.draw()
            self.tilesAcross = 1
            self.tileList.append(tile)
            sum_tile_width += 115

    def led_thread(self):
        prev_time = 0.0

        while True:
            # Clear background
            self.draw.rectangle((0, 0, self.displayWidth, self.height), fill=(0, 0, 0))

            for tile in self.tileList:
                if tile.x < self.displayWidth:  # Draw tile if onscreen
                    tile.draw()
                tile.x -= 1  # Move left 1 pixel
                if tile.x <= -tile.totalWidth:  # Off left edge?
                    tile.x = self.get_end_of_last_tile()  # Move off right &
                    if len(self.gameFetch.getGameInformations()) > 0:
                        games = self.gameFetch.getGameInformations()
                        tile.set_game(games[self.nextGameIndex])  # Update game
                        self.nextGameIndex += 1
                        if self.nextGameIndex >= len(games):
                            self.nextGameIndex = 0

            # Try to keep timing uniform-ish; rather than sleeping a fixed time,
            # interval since last frame is calculated, the gap time between this
            # and desired frames/sec determines sleep time...occasionally if busy
            # (e.g. polling server) there'll be no sleep at all.
            current_time = time.time()
            time_delta = (1.0 / self.fps) - (current_time - prev_time)
            if time_delta > 0.0:
                time.sleep(time_delta)
            prev_time = current_time

            # Offscreen buffer is copied to screen
            self.matrix.SetImage(self.image.im.id, 0, 0)

    def get_end_of_last_tile(self):
        if len(self.tileList) == 1:
            return self.displayWidth
        else:
            last_tile = self.tileList[0]
            for tile in self.tileList:
                if tile.x > last_tile.x:
                    last_tile = tile

            return last_tile.x + last_tile.totalWidth
