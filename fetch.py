import threading
import time
import urllib2
import base64
import json
import config


class GameFetch:
    interval = 60  # Default polling interval = 1 minute
    initSleep = 0  # Stagger polling threads to avoid load spikes
    hasLoaded = False

    def __init__(self):
        self.games = []
        self.lastQueryTime = time.time()
        t = threading.Thread(target=self.thread)
        t.daemon = True
        t.start()

    # Periodically get schedule from server ---------------------------
    def thread(self):
        init_sleep = GameFetch.initSleep
        GameFetch.initSleep += 5  # Thread staggering may
        time.sleep(init_sleep)  # drift over time, no problem
        while True:
            json_response = self.req()
            self.lastQueryTime = time.time()

            if json_response is not None:  # none means connection error
                if len(json_response) > 0:
                    game_list = []

                    for display_information in json_response:
                        game_list.append(GameInformation(display_information))

                    self.games = game_list  # Replace current list

            time.sleep(GameFetch.interval)

    # Open URL, send request, read & parse JSON response ------------------
    def req(self):
        print time.strftime("%H:%M:%S") + ": Fetching most recent scores..."
        json_output = None
        try:
            request = urllib2.Request(config.serverAddress)

            username = config.username
            password = config.password
            base_64_string = base64.encodestring('%s:%s' % (username, password)).replace('\n', '')
            request.add_header("Authorization", "Basic %s" % base_64_string)

            connection = urllib2.urlopen(request)
            json_output = json.load(connection)
            connection.close()
            self.hasLoaded = True
        finally:
            return json_output

    def getGameInformations(self):
        return self.games


class GameInformation:
    def __init__(self, information_json):
        self.away_team = information_json['awayTeamAbbreviation']
        self.home_team = information_json['homeTeamAbbreviation']

        self.away_color = self.get_color(self.away_team)
        self.home_color = self.get_color(self.home_team)

        self.away_score = None
        self.home_score = None
        self.away_shots = None
        self.home_shots = None
        self.away_pim = None
        self.home_pim = None
        self.away_hits = None
        self.home_hits = None
        self.away_power_play_percentage = None
        self.home_power_play_percentage = None
        self.away_power_play_goals = None
        self.home_power_play_goals = None
        self.away_face_off_percentage = None
        self.home_face_off_percentage = None
        self.away_take_aways = None
        self.home_take_aways = None
        self.away_give_aways = None
        self.home_give_aways = None

        if 'awayTeamScore' in information_json and 'homeTeamScore' in information_json:
            self.away_score = information_json['awayTeamScore']
            self.home_score = information_json['homeTeamScore']

        if 'awayTeamShots' in information_json and 'homeTeamShots' in information_json:
            self.away_shots = information_json['awayTeamShots']
            self.home_shots = information_json['homeTeamShots']

        if 'awayTeamPenaltyMinutes' in information_json and 'homeTeamPenaltyMinutes' in information_json:
            self.away_pim = information_json['awayTeamPenaltyMinutes']
            self.home_pim = information_json['homeTeamPenaltyMinutes']

        if 'awayTeamHits' in information_json and 'homeTeamHits' in information_json:
            self.away_hits = information_json['awayTeamHits']
            self.home_hits = information_json['homeTeamHits']

        if 'awayTeamPowerPlayPercentage' in information_json and 'homeTeamPowerPlayPercentage' in information_json:
            self.away_power_play_percentage = information_json['awayTeamPowerPlayPercentage']
            self.home_power_play_percentage = information_json['homeTeamPowerPlayPercentage']

        if 'awayTeamPowerPlayGoals' in information_json and 'homeTeamPowerPlayGoals' in information_json:
            self.away_power_play_goals = information_json['awayTeamPowerPlayGoals']
            self.home_power_play_goals = information_json['homeTeamPowerPlayGoals']

        if 'awayTeamPowerPlayOpportunities' in information_json and 'homeTeamPowerPlayOpportunities' in information_json:
            self.away_power_play_opportunities = information_json['awayTeamPowerPlayOpportunities']
            self.home_power_play_opportunities = information_json['homeTeamPowerPlayOpportunities']

        if 'awayTeamFaceOffPercentage' in information_json and 'homeTeamFaceOffPercentage' in information_json:
            self.away_face_off_percentage = information_json['awayTeamFaceOffPercentage']
            self.home_face_off_percentage = information_json['homeTeamFaceOffPercentage']

        if 'awayTeamBlockedShots' in information_json and 'homeTeamBlockedShots' in information_json:
            self.away_blocked_shots = information_json['awayTeamBlockedShots']
            self.home_blocked_shots = information_json['homeTeamBlockedShots']

        if 'awayTeamTakeAways' in information_json and 'homeTeamTakeAways' in information_json:
            self.away_take_aways = information_json['awayTeamTakeAways']
            self.home_take_aways = information_json['homeTeamTakeAways']

        if 'awayTeamGiveAways' in information_json and 'homeTeamGiveAways' in information_json:
            self.away_give_aways = information_json['awayTeamGiveAways']
            self.home_give_aways = information_json['homeTeamGiveAways']

        self.period = information_json['period']

    @staticmethod
    def get_color(abbreviation):
        if abbreviation == 'ANA':
            return 239, 82, 37
        if abbreviation == 'ARI':
            return 132, 31, 39
        if abbreviation == 'BOS':
            return 255, 196, 34
        if abbreviation == 'BUF':
            return 0, 46, 98
        if abbreviation == 'CGY':
            return 225, 58, 62
        if abbreviation == 'CAR':
            return 224, 58, 62
        if abbreviation == 'CHI':
            return 227 ,38, 58
        if abbreviation == 'COL':
            return 139, 41, 66
        if abbreviation == 'CBJ':
            return 0, 40, 92
        if abbreviation == 'DAL':
            return 0, 106, 78
        if abbreviation == 'DET':
            return 200, 0, 0
        if abbreviation == 'EDM':
            return 0, 55, 119
        if abbreviation == 'FLA':
            return 200, 33, 63
        if abbreviation == 'LAK':
            return 175, 183, 186
        if abbreviation == 'MIN':
            return 2, 87, 54
        if abbreviation == 'MTL':
            return 200, 20, 20
        if abbreviation == 'NSH':
            return 253, 187, 47
        if abbreviation == 'NJD':
            return 200, 0, 0
        if abbreviation == 'NYI':
            return 245, 125, 49
        if abbreviation == 'NYR':
            return 1, 97, 171
        if abbreviation == 'OTT':
            return 228, 23, 62
        if abbreviation == 'PHI':
            return 244, 121, 64
        if abbreviation == 'PIT':
            return 255, 255, 100
        if abbreviation == 'SJS':
            return 5, 83, 93
        if abbreviation == 'STL':
            return 5, 70, 160
        if abbreviation == 'TBL':
            return 1, 62, 125
        if abbreviation == 'TOR':
            return 0, 55, 119
        if abbreviation == 'VAN':
            return 4, 122, 74
        if abbreviation == 'WSH':
            return 207, 19, 43
        if abbreviation == 'WPG':
            return 1, 104, 171
        else:
            return 200, 200, 200
