# NextBus scrolling marquee display for Adafruit RGB LED matrix (64x32).
# Requires rgbmatrix.so library: github.com/adafruit/rpi-rgb-led-matrix

import atexit
import Image
import ImageDraw
import ImageFont
import time
import math
from fetch import GameFetch
from rgbmatrix import Adafruit_RGBmatrix

# Configurable stuff ---------------------------------------------------------

displayWidth = 265  # Matrix size (pixels) -- change for different matrix
height = 32  # types (incl. tiling).  Other code may need tweaks.
matrix = Adafruit_RGBmatrix(32, 8)  # rows, chain length
fps = 20  # Scrolling speed (ish)

# Main application -----------------------------------------------------------
titleFont = ImageFont.truetype("cour.ttf", 12)
font = ImageFont.truetype("cour.ttf", 16)
boldFont = ImageFont.truetype("courbd.ttf", 16)
fontYOffset = -4  # Scoot up a couple lines so descenders aren't cropped
image = Image.new('RGB', (displayWidth, height))
draw = ImageDraw.Draw(image)
currentTime = 0.0
prevTime = 0.0


# Clear matrix on exit.  Otherwise it's annoying if you need to break and
# fiddle with some code while LEDs are blinding you.
def clear_on_exit():
    matrix.Clear()

atexit.register(clear_on_exit)

# gamesFetch = GameFetch('2016-04-09')
gamesFetch = GameFetch(time.strftime("%Y-%m-%d"))

class Tile():
    grey = (100, 100, 100)
    white = (200, 200, 200)

    def __init__(self, starting_x, game, index):
        self.x = starting_x
        self.game = game
        self.index = index
        self.totalWidth = 20

    def draw(self):
        x = self.x
        y = 0

        #
        # Print Header
        #
        label = '      G  S'  # three letters, space, goals, two spaces, shots
        draw.text((x, y-2), label, font=titleFont, fill=self.grey)
        y += titleFont.getsize(label)[1]-1

        #
        # Print scores
        #
        label = self.game.away_team + \
                ' ' + \
                str(self.game.away_score) + \
                ' ' + str(self.game.away_shots)  # away team score
        if self.game.winner == 'away':
            draw.text((x, y + fontYOffset), label, font=boldFont,
                      fill=self.game.away_color)
        else:
            draw.text((x, y + fontYOffset), label, font=font,
                      fill=self.game.away_color)

        y += font.getsize(label)[1]-1  # push the teams a little closer together

        label = self.game.home_team + \
                ' ' + \
                str(self.game.home_score) + \
                ' ' + str(self.game.home_shots)  # home team score

        if self.game.winner == 'home':
            draw.text((x, y + fontYOffset - 1), label, font=boldFont,
                      fill=self.game.home_color)
        else:
            draw.text((x, y + fontYOffset - 1), label, font=font,
                      fill=self.game.home_color)

        self.totalWidth += boldFont.getsize(label)[0]+5

        #
        # Print Period Information
        #
        x += font.getsize(label)[0]+5
        y = int(math.ceil(font.getsize(label)[1]/2))
        label = "0"
        draw.text((x, y + fontYOffset), label, font=font, fill=self.white)
        self.totalWidth += font.getsize(label)[0]


class NoGameTile():
    grey = (100, 100, 100)
    white = (200, 200, 200)

    def __init__(self):
        self.totalWidth = 20
        self.x = 0

    def draw(self):
        x = self.x
        y = 5

        #
        # Print Header
        #
        label = 'No Games Today'
        draw.text((x, y), label, font=font, fill=self.grey)
        self.totalWidth += font.getsize(label)[0] + 5


while not gamesFetch.hasLoaded:
    time.sleep(1)  # let's wait for a response

# Allocate list of tile objects, enough to cover screen while scrolling
tileList = []
sumTileWidth = 0
nextGameIndex = 0
tileWidth = 115
tilesAcross = 0

if len(gamesFetch.getGameInformations()) > 0:
    # make a tile for each game
    for game in gamesFetch.getGameInformations():
        tile = Tile(sumTileWidth, game, nextGameIndex)
        tile.draw()
        nextGameIndex += 1
        tilesAcross += 1
        tileList.append(tile)
        sumTileWidth += tileWidth
        if sumTileWidth-tileWidth > displayWidth:
            break
else:
    tile = NoGameTile()
    tile.draw()
    tilesAcross = 1
    tileList.append(tile)
    sumTileWidth += tileWidth

# Initialization done; loop forever ------------------------------------------
while True:

    # Clear background
    draw.rectangle((0, 0, displayWidth, height), fill=(0, 0, 0))

    for t in tileList:
        if t.x < displayWidth:        # Draw tile if onscreen
            t.draw()
        t.x -= 1               # Move left 1 pixel
        if t.x <= -tileWidth:  # Off left edge?
            t.x = (tilesAcross-1) * tileWidth     # Move off right &
            if len(gamesFetch.getGameInformations()) > 0:
                games = gamesFetch.getGameInformations()
                t.game = games[nextGameIndex]  # Update game
                nextGameIndex += 1
                if nextGameIndex >= len(games):
                    nextGameIndex = 0

    # Try to keep timing uniform-ish; rather than sleeping a fixed time,
    # interval since last frame is calculated, the gap time between this
    # and desired frames/sec determines sleep time...occasionally if busy
    # (e.g. polling server) there'll be no sleep at all.
    currentTime = time.time()
    timeDelta = (1.0 / fps) - (currentTime - prevTime)
    if timeDelta > 0.0:
        time.sleep(timeDelta)
    prevTime = currentTime

    # Offscreen buffer is copied to screen
    matrix.SetImage(image.im.id, 0, 0)
