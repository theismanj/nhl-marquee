import Image
import ImageDraw
import ImageFont
import math

titleFont = ImageFont.truetype("cour.ttf", 12)
font = ImageFont.truetype("cour.ttf", 16)
boldFont = ImageFont.truetype("courbd.ttf", 16)
fontYOffset = -4  # Scoot up a couple lines so descenders aren't cropped
xPad = 20  # padding to use so the Tiles are not on top of each other.


class GameDetailTile:
    def __init__(self, image, draw, starting_x, gameToStats, index):
        self.gameToStats = gameToStats
        self.x = starting_x + xPad/2  # put half of the x-padding to the left then the other half will be on the right.
        self.index = index
        self.totalWidth = xPad

    def draw(self):
        x = self.x
        y = 0


class GameOverviewTile:
    grey = (100, 100, 100)
    white = (200, 200, 200)

    def __init__(self, starting_x, game, index, draw, height):
        self.x = starting_x
        self.game = game
        self.index = index
        self.image_draw = draw
        self.height = height
        self.totalWidth = 0

    def draw(self):
        x = self.x
        y = 0

        #
        # Print Header
        #
        label = '    '  # padding in front before header
        if self.game.away_score is not None:
            label += '  G'
        if self.game.away_shots is not None:
            label += '  S '
        if self.game.away_pim is not None:
            label += '  PIM '
        if self.game.away_hits is not None:
            label += '  H '
        if self.game.away_power_play_percentage is not None:
            label += '    PP%  '
        if self.game.away_power_play_goals is not None:
            label += ' PPG'
        if self.game.away_face_off_percentage is not None:
            label += '   FO% '
        if self.game.away_take_aways is not None:
            label += '  TA'
        if self.game.away_give_aways is not None:
            label += '  GA'

        self.image_draw.text((x, -2), label, font=titleFont, fill=self.grey)
        y += titleFont.getsize(label)[1]-1

        #
        # Print scores
        #
        label = self.game.away_team
        if self.game.away_score is not None:
            label += ' ' + str(self.game.away_score)
        if self.game.away_shots is not None:
            label += ' ' + str(self.game.away_shots)
            if self.game.away_shots < 10:
                label += '  '
        if self.game.away_pim is not None:
            label += ' ' + str(self.game.away_pim)
            if self.game.away_pim < 10:
                label += '  '
        if self.game.away_hits is not None:
            label += ' ' + str(self.game.away_hits)
            if self.game.away_hits < 10:
                label += '  '
        if self.game.away_power_play_percentage is not None:
            if str(self.game.away_power_play_percentage) == "0.0":
                label += ' 00.0%'
            else:
                label += ' ' + str(self.game.away_power_play_percentage) + '%'
        if self.game.away_power_play_goals is not None:
            label += ' ' + str(self.game.away_power_play_goals)
        if self.game.away_face_off_percentage is not None:
            if str(self.game.away_face_off_percentage) == "0.0":
                label += ' 00.0%'
            else:
                label += ' ' + str(self.game.away_face_off_percentage)
        if self.game.away_take_aways is not None:
            label += ' ' + str(self.game.away_take_aways)
            if self.game.away_take_aways < 10:
                label += '   '
        if self.game.away_give_aways is not None:
            label += ' ' + str(self.game.away_give_aways)
            if self.game.away_give_aways < 10:
                label += '   '
        self.image_draw.text((x, 5), label, font=font, fill=self.game.away_color)

        max_width = font.getsize(label)[0]+5

        y += font.getsize(label)[1]-1  # push the teams a little closer together

        label = self.game.home_team
        if self.game.home_score is not None:
            label += ' ' + str(self.game.home_score)
        if self.game.home_shots is not None:
            label += ' ' + str(self.game.home_shots)
            if self.game.home_shots < 10:
                label += '  '
        if self.game.home_pim is not None:
            label += ' ' + str(self.game.home_pim)
            if self.game.home_pim < 10:
                label += '  '
        if self.game.home_hits is not None:
            label += ' ' + str(self.game.home_hits)
            if self.game.home_hits < 10:
                label += '  '
        if self.game.home_power_play_percentage is not None:
            if str(self.game.home_power_play_percentage) == "0.0":
                label += ' 00.0%'
            else:
                label += ' ' + str(self.game.home_power_play_percentage) + '%'
        if self.game.home_power_play_goals is not None:
            label += ' ' + str(self.game.home_power_play_goals)
        if self.game.home_face_off_percentage is not None:
            if str(self.game.home_face_off_percentage) == "0.0":
                label += ' 00.0%'
            else:
                label += ' ' + str(self.game.home_face_off_percentage)
        if self.game.home_take_aways is not None:
            label += ' ' + str(self.game.home_take_aways)
            if self.game.home_take_aways < 10:
                label += '   '
        if self.game.home_give_aways is not None:
            label += ' ' + str(self.game.home_give_aways)
            if self.game.home_give_aways < 10:
                label += '   '

        self.image_draw.text((x, 17), label, font=font, fill=self.game.home_color)

        if font.getsize(label)[0]+5 > max_width:
            max_width = font.getsize(label)[0]+5

        #
        # Print Period Information
        #
        x += max_width
        label = self.game.period
        self.image_draw.text((x, 13), label, font=titleFont, fill=self.grey)

    def set_game(self, game):
        self.game = game
        self.determine_width()

    def determine_width(self):
        #
        # Print scores
        #
        label = self.game.away_team
        if self.game.away_score is not None:
            label += ' ' + str(self.game.away_score)
        if self.game.away_shots is not None:
            label += ' ' + str(self.game.away_shots)
            if self.game.away_shots < 10:
                label += '  '
        if self.game.away_pim is not None:
            label += ' ' + str(self.game.away_pim)
            if self.game.away_pim < 10:
                label += '  '
        if self.game.away_hits is not None:
            label += ' ' + str(self.game.away_hits)
            if self.game.away_hits < 10:
                label += '  '
        if self.game.away_power_play_percentage is not None:
            if str(self.game.away_power_play_percentage) == "0.0":
                label += ' 00.0%'
            else:
                label += ' ' + str(self.game.away_power_play_percentage) + '%'
        if self.game.away_power_play_goals is not None:
            label += ' ' + str(self.game.away_power_play_goals)
        if self.game.away_face_off_percentage is not None:
            if str(self.game.away_face_off_percentage) == "0.0":
                label += ' 00.0%'
            else:
                label += ' ' + str(self.game.away_face_off_percentage)
        if self.game.away_take_aways is not None:
            label += ' ' + str(self.game.away_take_aways)
            if self.game.away_take_aways < 10:
                label += '   '
        if self.game.away_give_aways is not None:
            label += ' ' + str(self.game.away_give_aways)
            if self.game.away_give_aways < 10:
                label += '   '

        max_width = font.getsize(label)[0] + 5

        label = self.game.home_team
        if self.game.home_score is not None:
            label += ' ' + str(self.game.home_score)
        if self.game.home_shots is not None:
            label += ' ' + str(self.game.home_shots)
            if self.game.home_shots < 10:
                label += '  '
        if self.game.home_pim is not None:
            label += ' ' + str(self.game.home_pim)
            if self.game.home_pim < 10:
                label += '  '
        if self.game.home_hits is not None:
            label += ' ' + str(self.game.home_hits)
            if self.game.home_hits < 10:
                label += '  '
        if self.game.home_power_play_percentage is not None:
            if str(self.game.home_power_play_percentage) == "0.0":
                label += ' 00.0%'
            else:
                label += ' ' + str(self.game.home_power_play_percentage) + '%'
        if self.game.home_power_play_goals is not None:
            label += ' ' + str(self.game.home_power_play_goals)
        if self.game.home_face_off_percentage is not None:
            if str(self.game.home_face_off_percentage) == "0.0":
                label += ' 00.0%'
            else:
                label += ' ' + str(self.game.home_face_off_percentage)
        if self.game.home_take_aways is not None:
            label += ' ' + str(self.game.home_take_aways)
            if self.game.home_take_aways < 10:
                label += '   '
        if self.game.home_give_aways is not None:
            label += ' ' + str(self.game.home_give_aways)
            if self.game.home_give_aways < 10:
                label += '   '

        if font.getsize(label)[0] + 5 > max_width:
            max_width = font.getsize(label)[0] + 5

        #
        # Print Period Information
        #
        label = self.game.period

        self.totalWidth = max_width + titleFont.getsize(label)[0] + 15  # Add 15 to total width for padding

class NoGameTile:
    grey = (100, 100, 100)
    white = (200, 200, 200)

    def __init__(self):
        self.totalWidth = 20
        self.x = 0

    def draw(self):
        x = self.x
        y = 5

        #
        # Print Header
        #
        label = 'No Games Today'
        draw.text((x, y), label, font=font, fill=self.grey)
        self.totalWidth += font.getsize(label)[0] + 5
