from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import json
from display import LEDOutput

display = LEDOutput()


class TestHTTPServerRequestHandler(BaseHTTPRequestHandler):

    # GET
    def do_GET(self):
        # Send response status code
        self.send_response(200)

        # Send headers
        self.send_header('Content-type', 'text/html')
        self.end_headers()

        # Send message back to client
        message = 'Hello World!'
        self.wfile.write(bytes(message, "utf8"))
        return

    def do_POST(self):
        data = None
        if int(self.headers['content-length']) > 0:
            data = self.rfile.read(int(self.headers['content-length']))

        games = json.loads(data)
        display.set_games(games)

        # Send response status code
        self.send_response(202)

        # Send headers
        self.send_header('Content-type', 'text/html')
        self.end_headers()

        return


def run():
    print('Starting server...')

    # Server settings
    # Choose port 8081, for port 80, which is normally used for a http server you need root access
    server_address = ('127.0.0.1', 8081)
    httpd = HTTPServer(server_address, TestHTTPServerRequestHandler)
    print('Running server...')

    httpd.serve_forever()


run()
