# README #

This is a simple Python application that is expected to run on a RaspberryPi. The script will call to a server to get NHL game information and scrolls the information across a LED matrix. A video of this project running can be seen here: https://goo.gl/photos/3rWoXmv5YWh7nmhV9.

### How do I get set up? ###

This project requires the rpi-rgb-led-matrix library that can be found at https://github.com/hzeller/rpi-rgb-led-matrix. A server must also be setup, for the Raspberry Pi to fetch the information from. Example code for this server can be found at https://bitbucket.org/theismanj/scoreboard-server/overview. The configuration (URL for the server, username, and password) should be placed in a config.py file. The parameters in this file should be "serverAddress" which is the address of the server that the pi will call to get the information, "username" which is the username setup on the server to prevent anyone from setting and getting information, and "password" which is the password for the provided username.