# NextBus scrolling marquee display for Adafruit RGB LED matrix (64x32).
# Requires rgbmatrix.so library: github.com/adafruit/rpi-rgb-led-matrix

import atexit
import Image
import ImageDraw
import ImageFont
import time
import math
from fetch import GameFetch
from rgbmatrix import Adafruit_RGBmatrix
import threading

# Configurable stuff ---------------------------------------------------------
displayWidth = 128  # Matrix size (pixels) -- change for different matrix
height = 32  # types (incl. tiling).  Other code may need tweaks.
matrix = Adafruit_RGBmatrix(32, 4)  # rows, chain length
fps = 20  # Scrolling speed (ish)

# Main application -----------------------------------------------------------
titleFont = ImageFont.truetype("cour.ttf", 12)
font = ImageFont.truetype("cour.ttf", 16)
boldFont = ImageFont.truetype("courbd.ttf", 16)
fontYOffset = -4  # Scoot up a couple lines so descenders aren't cropped
image = Image.new('RGB', (displayWidth, height))
draw = ImageDraw.Draw(image)
tileWidth = 115


class Tile:
    grey = (100, 100, 100)
    white = (200, 200, 200)

    def __init__(self, starting_x, game, index):
        self.x = starting_x
        self.game = game
        self.index = index
        self.totalWidth = 20

    def draw(self):
        x = self.x
        y = 0
        awayY = 6
        homeY = 18

        #
        # Print Header
        #
        label = '      G  S'  # three letters, space, goals, two spaces, shots
        draw.text((x, -2), label, font=titleFont, fill=self.grey)
        y += titleFont.getsize(label)[1]-1

        #
        # Print scores
        #
        label = self.game.away_team + \
                ' ' + \
                str(self.game.away_score) + \
                ' ' + str(self.game.away_shots)  # away team score
        if self.game.winner == 'away':
            draw.text((x, awayY), label, font=boldFont,
                      fill=self.game.away_color)
        else:
            draw.text((x, awayY), label, font=font,
                      fill=self.game.away_color)

        y += font.getsize(label)[1]-1  # push the teams a little closer together

        label = self.game.home_team + \
                ' ' + \
                str(self.game.home_score) + \
                ' ' + str(self.game.home_shots)  # home team score

        if self.game.winner == 'home':
            draw.text((x, homeY), label, font=boldFont,
                      fill=self.game.home_color)
        else:
            draw.text((x, homeY), label, font=font,
                      fill=self.game.home_color)

        self.totalWidth += boldFont.getsize(label)[0]+5

        #
        # Print Period Information
        #
        x += font.getsize(label)[0]+5
        y = int(awayY + math.ceil((homeY-awayY)/2)+1)
        label = self.game.period
        draw.text((x, y + fontYOffset), label, font=font, fill=self.white)
        self.totalWidth += font.getsize(label)[0]


class NHLOverviewOutput:
    def __init__(self):
        atexit.register(NHLOverviewOutput.clear_on_exit)
        self.thread = threading.Thread(target=NHLOverviewOutput.thread)
        self.thread.daemon = True
        self.thread.start()

    @staticmethod
    def thread():
        tileList = []
        sumTileWidth = 0
        nextGameIndex = 0
        tilesAcross = 0
        prevTime = 0.0
        gamesFetch = GameFetch('2016-04-09')
        while not gamesFetch.hasLoaded:
            time.sleep(1)  # let's wait for a response

        # make a tile for each game
        for game in gamesFetch.getGameInformations():
            tile = Tile(sumTileWidth, game, nextGameIndex)
            tile.draw()
            nextGameIndex += 1
            tilesAcross += 1
            tileList.append(tile)
            sumTileWidth += tileWidth
            if sumTileWidth-tileWidth > displayWidth:
                break

        # keep updating the display
        while True:
            # Clear background
            draw.rectangle((0, 0, displayWidth, height), fill=(0, 0, 0))

            for t in tileList:
                if t.x < displayWidth:        # Draw tile if onscreen
                    t.draw()
                t.x -= 1               # Move left 1 pixel
                if t.x <= -tileWidth:  # Off left edge?
                    t.x = (tilesAcross-1) * tileWidth     # Move off right &
                    games = gamesFetch.getGameInformations()
                    t.game = games[nextGameIndex]  # Update game
                    nextGameIndex += 1
                    if nextGameIndex >= len(games):
                        nextGameIndex = 0

            # Try to keep timing uniform-ish; rather than sleeping a fixed time,
            # interval since last frame is calculated, the gap time between this
            # and desired frames/sec determines sleep time...occasionally if busy
            # (e.g. polling server) there'll be no sleep at all.
            current_time = time.time()
            time_delta = (1.0 / fps) - (current_time - prevTime)
            if time_delta > 0.0:
                time.sleep(time_delta)
            prevTime = current_time

            # Offscreen buffer is copied to screen
            matrix.SetImage(image.im.id, 0, 0)
        gamesFetch.stop()

    # Clear matrix on exit.  Otherwise it's annoying if you need to break and
    # fiddle with some code while LEDs are blinding you.
    @staticmethod
    def clear_on_exit():
        matrix.Clear()
